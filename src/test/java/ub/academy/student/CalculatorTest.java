package ub.academy.student;

import org.junit.Before;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    Calculator cal;

    @Before
    public void setup() {
        cal = new Calculator();
    }

    public void testSum() {
        assertEquals(5, cal.sum(2, 3));
    }
}